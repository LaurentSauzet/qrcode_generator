const form = document.getElementById('generate-form');
const qr = document.getElementById('qrcode');


// Boutton de soumission du formulaire
const onGenerateSubmit = (e) => {
    e.preventDefault();

    clearUI()

    const url = document.getElementById('url').value;
    const size = document.getElementById('size').value;

    // Simple validation de l'url
    if (url === '') {
        alert('Please enter a URL');
    } else {
        showSpinner()
        // Affichage du spinner
        setTimeout(() => {
            hideSpinner();
            generateQRCode(url, size);

            // Génération du bouton d'enregsitrement après que l'image du qr code soit prête
            setTimeout(() => {
                // Récupération de l'url
                const saveURL = qr.querySelector('img').src;
                // Création du bouton
                createSaveBtn(saveURL);
            }, 50);
        }, 1000);
    }
};

// Génération du QR Code
const generateQRCode = (url, size) => {
    const qrcode = new QRCode('qrcode', {
        text: url,
        width: size,
        height: size,
    })
};

// Suppression du QR Code et du bouton d'enregistrement
const clearUI = () => {
    qr.innerHTML = '';
    const saveBtn = document.getElementById('save-link');
    if (saveBtn) {
        saveBtn.remove();
    }
}

// Affichage du spinner
const showSpinner = () => {
    const spinner = document.getElementById('spinner');
    spinner.style.display = 'block';
}

// Masquage du spinner
const hideSpinner = () => {
    const spinner = document.getElementById('spinner');
    spinner.style.display = 'none';
}

//
const createSaveBtn = (saveURL) => {
    const link = document.createElement('a');
    link.id = 'save-link';
    link.classList = 'bg-red-500 hover:bg-red-700 text-white font-bold py-2 rounded w-1/3 m-auto my-5';
    link.href = saveURL;
    link.download = 'qrcode';
    link.innerHTML = 'Enregistrer le QR Code';
    document.getElementById('generated').appendChild(link);
};

hideSpinner()

form.addEventListener('submit', onGenerateSubmit);



